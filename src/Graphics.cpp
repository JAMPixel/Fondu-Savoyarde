#include "Graphics.h"

#include <memory>
#include <array>

#include <GL/glew.h>

#include <glk/gl/SpriteAttributes.h>

#include "Programs.h"

std::unique_ptr<Graphics> graphics;

namespace {
	std::array<glk::gl::VertexCoord, 4> const unitQuad{{
		                                        {{0.0f, 0.0f}},
		                                        {{1.0f, 0.0f}},
		                                        {{0.0f, 1.0f}},
		                                        {{1.0f, 1.0f}}
	                                        }};
}

Graphics::Graphics()
: unitVbo{glk::gl::VboUsage::staticDraw, begin(unitQuad), end(unitQuad)}
, ropesVbo{glk::gl::VboUsage::dynamicDraw}
, dq{{unitVbo, programs->sprite.name()}} {
	TRY_GL(glBindVertexArray(ropesVao));
	TRY_GL(glBindBuffer(GL_ARRAY_BUFFER, ropesVbo.name()));
	glk::gl::enableVertexAttribs<RopeAttrib>(programs->rope.name());
	glk::gl::setVertexAttribPointers<RopeAttrib>(programs->rope.name());
	TRY_GL(glBindVertexArray(0u));
}
