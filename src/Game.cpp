#include <Perso.h>
#include "Game.h"

#include <iostream>

#include <GL/glew.h>

#include <SDL2/SDL_keyboard.h>

#include <glm/glm.hpp>

#include <glk/gl/Uniform.h>
#include <glk/gl/VertexAttribs.h>

#include "Map.h"
#include "Graphics.h"
#include "Programs.h"
#include "Resources.h"
#include "Colors.h"

std::unique_ptr<Game> game;

Game::Game() {
	map = loadMap("data/levels/0");
	perso = std::make_unique<Perso>(map->startPos,glm::vec2{0,0});
}

Game::~Game() {

}

void Game::loadNextLevel() {
	perso.reset();
	map = loadMap("data/levels/" + std::to_string(nextLevel));
	perso = std::make_unique<Perso>(map->startPos,glm::vec2{0,0});
	++nextLevel;
}

void Game::update() {
	perso->move2(map->loops);

	if(glm::length(perso->pos - map->endPos) < 1.0f)
		loadNextLevel();
}

void Game::toucheReleaseRope(int id){
	perso->releaseRope(id);
}

void Game::toucheLaunchRope(glm::vec2 sens, float defaultLong, const std::vector<std::vector<glm::vec2>> &walls, int id){

	perso->launchRope(sens,defaultLong,walls,id);
}

void Game::display() {
	map->display();

	glk_with(bind(programs->rope)) {
		static std::array<glm::vec4, 3> const cols{{
			colors::red, colors::blue, colors::yellow
		}};

		int c = -1;
		for(auto const &rope : perso->taMere) {
			++c;
			if(!rope)
				continue;
			graphics->ropesVbo.push_back(RopeAttrib{perso->pos, cols[c]});
			graphics->ropesVbo.push_back(RopeAttrib{rope->pos, cols[c]});
		}

		graphics->ropesVbo.push_back(RopeAttrib{map->startPos, colors::red});
		graphics->ropesVbo.push_back(RopeAttrib{map->endPos, colors::green});

		graphics->ropesVbo.upload();
		glk_with(bind(graphics->ropesVao), bind(graphics->ropesVbo, glk::gl::BufferTarget::array))
			TRY_GL(glDrawArrays(GL_LINES, 0u, graphics->ropesVbo.size()));
		graphics->ropesVbo.clear();
	}

	perso->display();

	auto &spr = programs->sprite;
	glk_with(bind(spr), bind(resources->tex.test, 0u)) {
		spr.spritesheet = 0;
		graphics->dq.perso.displayAndClear();
	}
}
