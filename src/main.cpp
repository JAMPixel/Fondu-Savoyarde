#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_events.h>

#include <GL/glew.h>

#include <glk/gl/util.h>
#include <glk/gl/Sprite.h>

#include "Graphics.h"
#include "Resources.h"
#include "Constants.h"
#include "Game.h"
#include "Programs.h"

#include "Graphics.h"
#include "Map.h"

int main(int, char **) {

	std::ios::sync_with_stdio(false);

	auto sdlWindow = SDL_CreateWindow(
		"Fondue Savoyarde",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		constants::W_WIDTH,
		constants::W_HEIGHT,
		SDL_WINDOW_OPENGL
	);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	auto glContext = SDL_GL_CreateContext(sdlWindow);
	assert(glContext);
	SDL_GL_MakeCurrent(sdlWindow, glContext);

	SDL_ShowWindow(sdlWindow);

	glewExperimental = true;
	GLenum glewStatus =glewInit();
	if(glewStatus != GLEW_OK) {
		std::cout << "Could not initialize GLEW, error : " << glewGetErrorString(glewStatus) << '\n';
		return 1;
	}

	(void)glGetError();

	{
		int maj, min;
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &maj);
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &min);
		std::cout << "Initialized OpenGL " << maj << "." << min << '\n';
	}

	// For 1 byte-per-pixel textures
	TRY_GL(glPixelStorei(GL_PACK_ALIGNMENT, 1));
	TRY_GL(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

	TRY_GL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

	GLint maxTextureUnits;
	TRY_GL(glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextureUnits));

	GLuint sampler;
	TRY_GL(glGenSamplers(1, &sampler));
	TRY_GL(glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	TRY_GL(glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST));

	for(int i = 0u; i < maxTextureUnits; ++i) {
		TRY_GL(glActiveTexture(GL_TEXTURE0 + i));
		TRY_GL(glBindSampler(i, sampler));
	}

	programs = std::make_unique<Programs>();
	graphics = std::make_unique<Graphics>();
	resources = std::make_unique<Resources>();
	game = std::make_unique<Game>();

	std::uint32_t timer = SDL_GetTicks();
	SDL_Event ev;

	/* variables */
	int indiceNormal = 0;
	int indiceTouche = 0;
	int testAppuie = 1000;
	int souriX = 0;
	int souriY = 0;

    int tabIndice[3];
    tabIndice[0]=testAppuie;
    tabIndice[1]=testAppuie;
    tabIndice[2]=testAppuie;

	bool loop = true;
    bool pause = false;
	do {
		while(SDL_PollEvent(&ev)) {
			switch (ev.type){
				case SDL_QUIT:
					loop = false;
					break;
				case SDL_KEYDOWN:
					switch (ev.key.keysym.scancode){
						case SDL_SCANCODE_ESCAPE:
							loop = false;
							break;
						case SDL_SCANCODE_E:
							indiceTouche = 0;
							if ( indiceNormal == 3 )
                            	game->toucheLaunchRope(glm::vec2(souriX, souriY),tabIndice[0]/100,(game->map)->loops,0);

							break;
                        case SDL_SCANCODE_W:
                            indiceTouche = 1;
							if (indiceNormal == 3 )
                            	game->toucheLaunchRope(glm::vec2(souriX, souriY),tabIndice[1]/100,(game->map)->loops,1);

                            break;
                        case SDL_SCANCODE_Q:
                            indiceTouche = 2;
							if (indiceNormal == 3 )
                            	game->toucheLaunchRope(glm::vec2(souriX, souriY),tabIndice[2]/100,(game->map)->loops,2);
                            break;
						case SDL_SCANCODE_R:
							std::cout << "couper la corde d'indice " << indiceTouche << std::endl;

                            switch (indiceTouche){
                                case 0:
                                    game->toucheReleaseRope(0);
                                    break;
                                case 1:
                                    game->toucheReleaseRope(1);
                                    break;
                                case 2:
                                    game->toucheReleaseRope(2);
                                    break;

                            }
                            break;
                        case SDL_SCANCODE_P:
                            pause = !pause;
                            break;
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					indiceNormal = 3;

					break;
				case SDL_MOUSEBUTTONUP:
					indiceNormal = 100;
                    std::cout << "Raideur de la touche " << indiceTouche <<" du ressor : " << tabIndice[indiceTouche]/100 << std::endl;
					break;
			}
		}

		SDL_GetMouseState(&souriX, &souriY);

		if(indiceNormal == 3) {
            switch (indiceTouche){
                case 0:
                    tabIndice[0]--;
                    if(tabIndice[0] <= 0){
                        tabIndice[0] = 1;
                    }
                    break;
                case 1:
                    tabIndice[1]--;
                    if(tabIndice[1] <= 0){
                        tabIndice[1] = 1;
                    }
                    break;
                case 2:
                    tabIndice[2]--;
                    if(tabIndice[2] <= 0){
                        tabIndice[2] = 1;
                    }
                    break;
            }
		}
		else{
            tabIndice[0] = 1000;
            tabIndice[1] = 1000;
            tabIndice[2] = 1000;
			indiceNormal = 0;
		}


		TRY_GL(glClear(GL_COLOR_BUFFER_BIT));
		TRY_GL(glEnable(GL_BLEND));
		TRY_GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
        if(!pause)
		    game->update();
		game->display();

		SDL_GL_SwapWindow(sdlWindow);

		std::uint32_t frameTime = SDL_GetTicks() - timer;
		if(frameTime < 16u)
			SDL_Delay(16u - frameTime);
		timer += frameTime;
	} while(loop);

	game.reset();
	graphics.reset();
	resources.reset();
	programs.reset();

	TRY_GL(glDeleteSamplers(1, &sampler));

	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(sdlWindow);
	SDL_Quit();

	return 0;
}
