//
// Created by hole on 06/05/16.
//

#include <glm/vec2.hpp>
#include <vector>
#include <algorithm>
#include <glm/glm.hpp>
#include <iostream>

std::vector<glm::vec2> triangulisationnage(std::vector<std::vector<glm::vec2>> boucles, float w, float h) {
    std::vector<glm::vec2> triangles;
    auto min = std::min_element(begin(boucles[0]),end(boucles[0]),[](glm::vec2 v1,glm::vec2 v2){return(glm::length(v1)<glm::length(v2)); });
    boucles[0].insert(min,{*min,{0,0},{0,h},{w,h},{w,0}});

    for(auto b:boucles){
        b.pop_back();
        //std::cout << "coucou" <<std::endl;
        bool pas_fini = true;
        while(pas_fini) {

            //std::cout << "c'est partit" <<std::endl;
            pas_fini = false;
            for (auto i = begin(b); i != end(b);) {
                glm::vec2 p1 = *i;
                glm::vec2 p2;
                glm::vec2 p3;
                //std::cout << i - begin(b)<<" ";

                if(i+1 == end(b)){
                    p2 = b.front();
                    p3 = b[1];
                //    std::cout << "bbbb  ";
                    if((p2 - p1).x * (p3 - p2).y - (p3 - p2).x * (p2 - p1).y < 0){
                        triangles.push_back(p1);
                        triangles.push_back(p2);
                        triangles.push_back(p3);
                        b.erase(b.begin());
                        i = b.end()-1;
                        pas_fini = true;}else{i++;}
                }else{
                    p2 = i[1];
                    if(i+2==end(b)){
                        p3 = b[0];
          //              std::cout << "cccc  ";
                        if((p2 - p1).x * (p3 - p2).y - (p3 - p2).x * (p2 - p1).y < 0) {
                            triangles.push_back(p1);
                            triangles.push_back(p2);
                            triangles.push_back(p3);
                            b.erase(i + 1);
                            pas_fini = true;
                        }
                        ++i;
                    }else{
                        p3 = i[2];
            //            std::cout << "dddd  ";
                            if((p2 - p1).x * (p3 - p2).y - (p3 - p2).x * (p2 - p1).y < 0) {
                                triangles.push_back(p1);
                                triangles.push_back(p2);
                                triangles.push_back(p3);
                                b.erase(i + 1);
                                pas_fini = true;
                            }
                        i++;
                    }
                }
              //  std::cout <<"x: "<< p1.x <<"   y: "<< p1.y <<"    "<<"x: "<< p2.x <<"   y: "<< p2.y<<"   "<<"x: "<< p3.x <<"   y: "<< p3.y <<"     "<<b.size() <<std::endl;



            }
        }
    }
    return triangles;
}
