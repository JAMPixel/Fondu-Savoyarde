#include "Map.h"

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <memory>

#include <GL/glew.h>

#include <glk/gl/Vbo.h>
#include <Programs.h>

#include "glAttribs.h"
#include "Programs.h"
#include "Constants.h"
#include "Segment/triangule.hpp"
#include <numeric>

Map::Map(std::vector<std::vector<glm::vec2>> loops_, glm::vec2 startPos, glm::vec2 endPos)
: loops{loops_}
, vbo{glk::gl::VboUsage::staticDraw}
, startPos{startPos}
, endPos{endPos} {

	TRY_GL(glBindVertexArray(vao));
	TRY_GL(glBindBuffer(GL_ARRAY_BUFFER, vbo.name()));
	glk::gl::enableVertexAttribs<glm::vec2>(programs->line.name(), "pos");
	glk::gl::setVertexAttribPointers<glm::vec2>(programs->line.name(), "pos");

//	std::vector<glm::vec2> tris = triangulisationnage(loops_, 32, 20);
//
//	vbo.assign(begin(tris), end(tris));

	GLsizeiptr numSom = std::accumulate(
		begin(loops),
		end(loops),
		GLsizeiptr{0},
		[](GLsizeiptr acc, auto const &vec) { return acc + vec.size(); }
	);

	vbo.reserve(numSom);
	TRY_GL(glBindBuffer(GL_ARRAY_BUFFER, vbo.name()));

	{
		GLsizeiptr idx = 0u;
		for(auto const &vec : loops) {
			TRY_GL(glBufferSubData(GL_ARRAY_BUFFER, idx * sizeof(glm::vec2), vec.size() * sizeof(glm::vec2), vec.data()));
			idx += vec.size();
		}
	}
	TRY_GL(glBindVertexArray(0u));
}

void Map::display() {

	auto &prg = programs->line;
	glk_with(bind(prg), bind(vao)) {
		prg.color = glm::vec4{1.0f};
		TRY_GL(glDrawArrays(GL_LINE_STRIP, 0u, vbo.size()));
	}
}

std::unique_ptr<Map> loadMap(std::string const &fName) {
	std::ifstream ifs{fName};
	ifs.exceptions(std::ios::badbit);

	glm::vec2 startPos, endPos;

	if(!(ifs >> startPos.x >> startPos.y))
		throw std::runtime_error("Malformatted input : start position");

	if(!(ifs >> endPos.x >> endPos.y))
		throw std::runtime_error("Malformatted input : end position");

	std::vector<std::vector<glm::vec2>> loops;
	for (; ;) {
		std::vector<glm::vec2> loop;

		glm::vec2 pos;

		if(!(ifs >> pos.x >> pos.y))
			break;

		for (; ;) {
			loop.push_back(pos);

			if (!(ifs >> pos.x >> pos.y)) {
				ifs.clear();
				std::string end;
				// End of a loop ?
				if ((ifs >> end) && end == "end") {
					// yep
					loops.push_back(std::move(loop));
					break;
				}

				throw std::runtime_error("Malformatted input : loop end");
			}
		}
	}

	return std::make_unique<Map>(std::move(loops), startPos, endPos);
}
