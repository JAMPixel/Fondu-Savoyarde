#include "Programs.h"

#include <glk/gl/Shader.h>
#include <glk/path.h>
#include <glk/util.h>

#include "Programs.h"
#include "Constants.h"

std::unique_ptr<Programs> programs;

Programs::Programs()
	: line{
	glk::gl::VertShader{"data/map.vert"_f},
	glk::gl::FragShader{"data/map.frag"_f}
}
	, rope{
		glk::gl::VertShader{"data/rope.vert"_f},
		glk::gl::FragShader{"data/rope.frag"_f}
	}
	, sprite{
		glk::gl::defaultSpriteProgram()
	} {
	glm::mat3 const cam{
		2.0f * constants::PX_PER_METER / constants::W_WIDTH, 0.0f, 0.0f,
		0.0f, -2.0f * constants::PX_PER_METER / constants::W_HEIGHT, 0.0f,
		-1.0f, 1.0f, 1.0f
	};

	glk_with(bind(line))
		line.pvMatrix = cam;

	glk_with(bind(rope))
		rope.pvMatrix = cam;

	glk_with(bind(sprite))
		sprite.pvMatrix = cam;
}
