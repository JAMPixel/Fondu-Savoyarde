#include "Constants.h"

namespace constants {
	int const W_WIDTH = 1024, W_HEIGHT = 640;
	int const PX_PER_METER = 32;
	float const METERS_PER_PX = 1.0f / PX_PER_METER;
}
