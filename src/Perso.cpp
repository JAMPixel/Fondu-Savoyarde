//
// Created by unnom on 06/05/16.
//

#include "Perso.h"
#include <cassert>

#include "glm/glm.hpp"

#include "Graphics.h"
#include "Resources.h"
#include "Rope.h"

#include <cassert>
#include <cmath>
#include <iostream>
#include <Colors.h>
#include "Constants.h"
constexpr float massePerso = 0.1;
constexpr float masseMembre = 0.001;
constexpr float accelGrav = 0.01f*1.0f/60.0f;
const glm::vec2 poids{0, massePerso*accelGrav};
const glm::vec2 poidsMembre{0,masseMembre*accelGrav};
constexpr float distance = 0.05;
constexpr float frotSec = 0.003f;


Perso::Perso(glm::vec2 pos, glm::vec2 vit) : pos(pos), vit(vit), spr{&resources->tex.doll} {
    spr.origin = glm::vec2{32, 64};
    spr.scale = glm::vec2{1.0f / 128.0f};

     body[0] = Membre(&body[1]);
     body[1] = Membre(&body[0]);
     body[2] = Membre(&body[0]);
     body[3] = Membre(&body[0]);
     body[4] = Membre(&body[1]);
     body[5] = Membre(nullptr);
     body[0].pos = pos;
     for (int i = 1; i < body.size()-1; i++) {
         body[i].distance = distance;
         body[i].pos = body[i].poto->pos + glm::vec2(distance / (float) sqrt(2));

     }
    //taMere[1] = std::make_unique<Rope>(1, glm::vec2(5, 10));

}


void Perso::display() {
    spr.position = pos;
    graphics->dq.perso.enqueue(spr.makeAttribs());
}

void Perso::launchRope(glm::vec2 sens, float defaultLong, const std::vector<std::vector<glm::vec2>> &walls, int id) {
    time = 0;
    glm::vec2 attachPoint;
   // std::cout << "plop " << sens.y <<std::endl;
	static glm::vec4 const purple{0.5f, 0.0f, 1.0f, 1.0f};
    graphics->ropesVbo.push_back(RopeAttrib{{1.0f, 1.0f}, purple});
    graphics->ropesVbo.push_back(RopeAttrib{constants::METERS_PER_PX*sens, purple});

    if (pointLePlusPresDasnLaDirection(sens*constants::METERS_PER_PX, walls, attachPoint)) {
        taMere[id].reset();
        taMere[id] = std::make_unique<Rope>(defaultLong, attachPoint);

    }
}

void Perso::move(const std::vector<std::vector<glm::vec2>> &walls, int id) {
    glm::vec2 force = poids;
    if (taMere[id] != nullptr) {
        glm::vec2 vecCorde{taMere[id]->pos - pos};
        glm::vec2 ressort {taMere[id]->raideur*(glm::length(vecCorde)-taMere[id]->defaultLong)*glm::normalize(vecCorde)};
        if(glm::length(vecCorde) > taMere[id]->defaultLong)
            force += ressort;


    }
    vit += (force/massePerso) - frotSec*vit;


        glm::vec2 newPos = this->pos + 1.0f *vit;
        glm::vec2 coorWall;
    //std::cout<< vit.x << "   "<< vit.y<<std::endl;

    if (pointLePlusPresDasnLaDirection(vit+pos, walls, coorWall)) {

            //std::cout << coorWall.x << "  et " << coorWall.y << std::endl;
            //graphics->ropesVbo.push_back(VertexCoord{{coorWall.x,coorWall.y}});
            //graphics->ropesVbo.push_back(VertexCoord{{0,0}});

            glm::vec2 vitWall = coorWall - pos;
            glm::vec2 newPosWall = coorWall - newPos;
           // std::cout << glm::dot(newPosWall, vitWall) << std::endl;
            if (glm::dot(newPosWall, vitWall) <= 0  ) {
                if(pointSeg2.y < pointSeg1.y){
                    std::swap(pointSeg1, pointSeg2);
                }
                glm::vec2 vecWall = pointSeg2 - coorWall;
                vit = glm::dot(glm::normalize(vecWall),vit)*glm::normalize(vecWall);


                //this->pos = coorWall;

               // std::cout <<"pos"<< pos.y<<"  wall " << coorWall.y  << "vitesse" << vit.y<<std::endl;
             //   std::cout <<"plop"<< std::endl;

            } else {
                this->pos = newPos;

           //     std::cout <<"--";
            }
        } else {
            this->pos = newPos;
        }



}

void Perso::movePoint(const std::vector<std::vector<glm::vec2>> walls, int id) {

    if(body[id]. poto != nullptr) {
        glm::vec2 vecEntreLesDeuxPoints = body[id].pos - body[id].poto->pos;
        glm::vec2 ortho = glm::normalize(glm::vec2{vecEntreLesDeuxPoints.y, -vecEntreLesDeuxPoints.x});
        glm::vec2 force = ortho * glm::dot(poidsMembre, ortho);
        //force = poids;

        vit += force / massePerso;
        glm::vec2 newPos = body[id].pos + vit;
        glm::vec2 coorWall;
       /* if (pointLePlusPresDasnLaDirection(vit, walls, coorWall)) {
            glm::vec2 vitWall = coorWall - body[id].pos;
            glm::vec2 newPosWall = coorWall - newPos;

            if (glm::dot(newPosWall, vitWall) <= 0) {
                body[id].pos = coorWall;
            } else {
                body[id].pos = newPos;
            }
        } else {*/
            body[id].pos = newPos;
       // }
    }



}


void Perso::move2(const std::vector<std::vector<glm::vec2>> walls) {

  /*  for(int i = 0; i < body.size(); i++){
          movePoint(walls, i);
      }*/
    for (int i = 3; i < body.size(); i++) {
        move(walls, i - 3);
    }



}


void Perso::releaseRope(int id) {
    time = 0;
    taMere[id].reset();


}

bool Perso::pointLePlusPresDasnLaDirection(glm::vec2 direction, const std::vector<std::vector<glm::vec2>> &boucles,
                                           glm::vec2 &leFameuxPoint) {
    assert(!boucles.empty() && "le vector c'est mieux s'il n'est pas vide");
    float dist = 1.0f / 0.0f;// = inf, parceque la fleme des numeric limit

    auto prod = [](glm::vec2 a, glm::vec2 b) {
        return a.x * b.y - a.y * b.x;
    };

    for (auto b:boucles) {
        for (auto i = begin(b); i != end(b) - 1; i++) {
            glm::vec2 point1 = *i;
            glm::vec2 point2 = i[1];



            if (prod(point2 - point1, direction - pos) <= 0)
                continue;

            if(prod(point2 - point1, pos - point1) >= 0)
                continue;

            glm::vec2 pPrime = point1 + glm::dot((pos - point1), glm::normalize(point2 - point1)) * glm::normalize(point2 - point1);
            glm::vec2 pSeconde = pos + glm::dot(direction - pos, glm::normalize(pPrime - pos))  * glm::normalize(pPrime-pos);

            glm::vec2 p = pos + (direction - pos) * glm::length(pPrime - pos) / glm::length(pSeconde - pos);

            if (glm::length(p - pos) > dist || glm::dot(direction - pos, p - pos) < 0 ||
                glm::dot(point1 - p, point2 - p) >= 0)
                continue;

            dist = glm::length(p - pos);
            leFameuxPoint = p;
            pointSeg1 = point1;
            pointSeg2 = point2;

               // graphics->ropesVbo.push_back(RopeAttrib{{},{}});
                //graphics->ropesVbo.push_back(RopeAttrib{p,colors::red});


            /*if (((point2 - point1).x * (pos - point1).y - (pos - point1).x * (point2 - point1).y) > 0) {

               // graphics->ropesVbo.push_back(VertexCoord{{point1.x,point1.y}});
                //graphics->ropesVbo.push_back(VertexCoord{{point2.x,point2.y}});
               // graphics->ropesVbo.push_back(VertexCoord{{direction.x,direction.y}});
                //graphics->ropesVbo.push_back(VertexCoord{{0,0}});

                //std::cout<< direction.x - pos.x << "   "<< direction.y -pos.y<<std::endl;
                glm::vec2 pPrime =
                        point1 + glm::dot((pos - point1), (point2 - point1)/ glm::length(point2 - point1)) * (point2 - point1)/ glm::length(point2 - point1);
               // graphics->ropesVbo.push_back(VertexCoord{{pPrime.x,pPrime.y}});
                //graphics->ropesVbo.push_back(VertexCoord{{0,0}});

                glm::vec2 pSeconde = pos + glm::dot(direction - pos, (pPrime - pos)/glm::length(pPrime-pos))  * (pPrime-pos)/ glm::length(pPrime - pos);
             //   graphics->ropesVbo.push_back(VertexCoord{{pSeconde.x,pSeconde.y}});
               // graphics->ropesVbo.push_back(VertexCoord{{0,0}});

                glm::vec2 p = pos + ((direction - pos) / glm::length(direction - pos)) *
                                    glm::length(direction - pos) *
                                    glm::length(pPrime - pos) / glm::length(pSeconde - pos);



//                std::cout << ((direction - pos) / glm::length(direction - pos)).x << "  et " << ((direction - pos) / glm::length(direction - pos)).y<< std::endl;

                if (glm::length(p - pos) < dist && glm::dot(direction - pos, p - pos) >= 0 &&
                    glm::dot(point1 - p, point2 - p) < 0) {

                    dist = glm::length(p - pos);
                    leFameuxPoint = p;
                    pointSeg1 = point1;
                    pointSeg2 = point2;
                   // graphics->ropesVbo.push_back(VertexCoord{{pPrime.x,pPrime.y}});
                    //graphics->ropesVbo.push_back(VertexCoord{{0,0}});

                   // graphics->ropesVbo.push_back(VertexCoord{{pSeconde.x,pSeconde.y}});
                    //graphics->ropesVbo.push_back(VertexCoord{{0,0}});
                 //   std::cout<< pSeconde.x << "  p2 "<< pSeconde.y<<std::endl;


                    //  graphics->ropesVbo.push_back(VertexCoord{{direction.x,direction.y}});
                     // graphics->ropesVbo.push_back(VertexCoord{{0,0}});


                    graphics->ropesVbo.push_back(RopeAttrib{{p.x,p.y},glm::vec4{1.0f}});
                    graphics->ropesVbo.push_back(RopeAttrib{{0,0},glm::vec4{1.0f}});
                }
            }

        */}
    }
   // std::cout <<"   " <<dist<<"  "<<(dist < 1.0f/0.0f)<< std::endl;
    return (dist < 1.0f/0.0f);

}

