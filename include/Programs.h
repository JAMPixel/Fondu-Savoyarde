#pragma once

#include <memory>

#include <glk/gl/Program.h>
#include <glk/gl/SpriteProgram.h>

struct Programs {
	Programs();

	GLK_GL_PROGRAM(,
		(glm::mat3, pvMatrix)
		(glm::vec4, color)
	) line;

	GLK_GL_PROGRAM(,
		(glm::mat3, pvMatrix)
	) rope;

	glk::gl::SpriteProgram sprite;
};

extern std::unique_ptr<Programs> programs;
