#pragma once

namespace constants {
	extern int const W_WIDTH, W_HEIGHT;
	extern int const PX_PER_METER;
	extern float const METERS_PER_PX;
}
