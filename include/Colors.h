#pragma once

namespace colors {
	static glm::vec4 red{1.0f, 0.0f, 0.0f, 1.0f}, blue{0.0f, 0.0f, 1.0f, 1.0f}, yellow{1.0f, 1.0f, 0.0f, 1.0f}, green{0.0f, 1.0f, 0.0f, 1.0f};
}
