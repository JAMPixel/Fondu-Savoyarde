#pragma once

#include <memory>

#include <glk/gl/Texture.h>
#include <glk/gl/SpriteStrip.h>

struct Resources {
	Resources();

	struct {
		glk::gl::TextureName test;
		SpriteStrip doll{{64, 128}};
	} tex;
};

extern std::unique_ptr<Resources> resources;
