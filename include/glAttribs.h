#pragma once

#include <glk/gl/VertexAttribs.h>

GLK_GL_ATTRIB_STRUCT(SegmentAttrib, (glm::vec2, pos));

GLK_GL_ATTRIB_STRUCT(RopeAttrib, (glm::vec2, pos)(glm::vec4, col));
