#pragma once

#include <memory>
#include "Perso.h"

struct Map;

struct Game {
	Game();
	~Game();

	void update();
	void display();

	void toucheReleaseRope(int id);
	void toucheLaunchRope(glm::vec2 sens, float defaultLong, const std::vector<std::vector<glm::vec2>> &walls, int id);

	void loadNextLevel();

	std::unique_ptr<Perso> perso;
	std::unique_ptr<Map> map;

	int nextLevel = 0;
};

extern std::unique_ptr<Game> game;
