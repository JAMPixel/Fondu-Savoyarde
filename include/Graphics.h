#pragma once

#include <memory>

#include <glm/vec2.hpp>

#include <glk/gl/Vbo.h>
#include <glk/gl/CachedVbo.h>
#include <glk/gl/SpriteAttributes.h>
#include <glk/gl/InstanceQueue.h>

#include "glAttribs.h"

struct Graphics {
	Graphics();
	glk::gl::Vbo<glk::gl::VertexCoord> unitVbo;

	glk::gl::CachedVbo<RopeAttrib> ropesVbo;
	glk::gl::VaoName ropesVao;

	struct {
		glk::gl::InstanceQueue<glk::gl::VertexCoord, glk::gl::SpriteAttribs> perso;
	} dq;
};

extern std::unique_ptr<Graphics> graphics;