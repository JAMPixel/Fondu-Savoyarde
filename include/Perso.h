//
// Created by unnom on 06/05/16.
//

#ifndef LIMONADE_PERSO_H
#define LIMONADE_PERSO_H

#include <memory>
#include <vector>

#include "glm/glm.hpp"

#include <glk/gl/Sprite.h>

#include "Rope.h"
#include <array>


struct Membre{
    Membre * poto;
    float distance;
    glm::vec2 pos;
    glm::vec2 vit{0,0};
    Membre (){};
    Membre(Membre * poto):poto(poto){}
    void operator=(const Membre &&orig){poto = orig.poto; distance = orig.distance;}
};
struct Segment;

struct Perso{

    glm::vec2 pos;
    glm::vec2 vit{0,0};
    std::array<std::unique_ptr<Rope>, 3> taMere;
    float time = 0;
    std::array<Membre, 6> body;
	glk::gl::Sprite spr;
	glm::vec2 pointSeg1;
	glm::vec2 pointSeg2;


    Perso(glm::vec2 pos, glm::vec2 vit);
	void display();

    void launchRope(glm::vec2 sens, float defaultLong, const std::vector<std::vector<glm::vec2>> &walls, int id);
    void releaseRope(int id);
    void movePoint(const std::vector<std::vector<glm::vec2>>walls, int id);
    void move2(const std::vector<std::vector<glm::vec2>> walls);
    void move(const std::vector<std::vector<glm::vec2>> &walls, int id);
    bool pointLePlusPresDasnLaDirection(glm::vec2 direction, const std::vector<std::vector<glm::vec2>> & boucles, glm::vec2 & leFameuxPoint);

};
#endif //LIMONADE_PERSO_H
