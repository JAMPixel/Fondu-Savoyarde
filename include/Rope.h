//
// Created by unnom on 06/05/16.
//

#ifndef LIMONADE_ROOP_H
#define LIMONADE_ROOP_H

#include <glm/vec2.hpp>

struct Rope{
    glm::vec2 pos;
    glm::vec2 sens;
    float raideur = 0.0004;
    float defaultLong;

    Rope(int defaultLong, glm::vec2 pos);
};

#endif //LIMONADE_ROOP_H
