//
// Created by hole on 06/05/16.
//

#ifndef FONDUE_SAVOYARDE_TRIANGULE_HPP
#define FONDUE_SAVOYARDE_TRIANGULE_HPP
#include <glm/vec2.hpp>

std::vector<glm::vec2> triangulisationnage(std::vector<std::vector<glm::vec2>> boucles, float w, float h);

#endif //FONDUE_SAVOYARDE_TRIANGULE_HPP
