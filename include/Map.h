#pragma once

#include <vector>
#include <memory>

#include <glk/gl/Vbo.h>

#include "glAttribs.h"

struct Map {
	Map(std::vector<std::vector<glm::vec2>> loops_, glm::vec2 startPos, glm::vec2 endPos);

	void display();

	std::vector<std::vector<glm::vec2>> loops;
	glk::gl::Vbo<glm::vec2> vbo;
	glk::gl::VaoName vao;
	glm::vec2 startPos, endPos;
};

std::unique_ptr<Map> loadMap(std::string const &fName);
