#version 330

uniform mat3 pvMatrix;

in vec2 pos;

out VSOut {
	vec2 uv;
} _out;

void main() {
	gl_Position = vec4(
		pvMatrix * vec3(pos, 1.0),
		1.0
	);
}
