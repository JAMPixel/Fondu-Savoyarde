#version 330

uniform mat3 pvMatrix;

in vec2 pos;
in vec4 col;

out VSOut {
	vec4 col;
} _out;

void main() {
	gl_Position = vec4(
		pvMatrix * vec3(pos, 1.0),
		1.0
	);
	_out.col = col;
}
