#version 330

uniform vec4 color;

in VSOut {
	vec2 uv;
} _in;

out vec4 col;

void main() {
	col = color;
}
