#version 330

in VSOut {
	vec4 col;
} _in;

out vec4 col;

void main() {
	col = _in.col;
}
